const express = require('express')
const openai = require('../api/openai.js')
const login = require('../api/login.js')
const signin = require('../api/signin.js')

const router = express.Router()

router.post('/api/openai', openai)

router.post('/api/login', login)
router.post('/api/signin', signin)

router.get('/', (req, res) => {
    res.send('Hello Express')
})

module.exports = router
