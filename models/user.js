const mongoose = require('./mongo')
const bcryptjs = require('bcryptjs')

const UserSchema = new mongoose.Schema({
  username: { type: String, unique: true },
  password: {
    type: String,
    set (val) {
      return bcryptjs.hashSync(val, 10)
    },
    token: {
      type: Number,
      set (val) {
        return 100000
      }
    }
  }
})

const User = mongoose.model('User', UserSchema)

module.exports = User
