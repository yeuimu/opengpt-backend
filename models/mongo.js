const config = require('../config/config')
const mongoose = require('mongoose')

mongoose.connect(`${config.mongodb}/user`, {
  useNewUrlParser: true
}).catch((error) => {
  console.error(error.message)
})

module.exports = mongoose
