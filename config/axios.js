const { proxy, openkey, model } = require('./config')

module.exports = {
  method: 'post',
  baseURL: 'https://api.openai.com/v1/chat/completions',
  headers: {
    'Content-Type': 'application/json',
    Authorization: `Bearer ${openkey}`
  },
  proxy,
  responseType: 'stream',
  data: {
    model,
    messages: [
      {
        role: 'user',
        content: 'test'
      }
    ],
    stream: true
  }
}
