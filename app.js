const express = require('express');
const router = require('./router/index')
const { port, hostname } = require('./config/config')

const app = express();

// 跨域设置
app.all("*", function(req, res, next) {
	res.header("Access-Control-Allow-Origin", req.headers.origin || '*');
	res.header("Access-Control-Allow-Headers", "Content-Type, Authorization, X-Requested-With");
	res.header("Access-Control-Allow-Methods", "PUT,POST,GET,DELETE,OPTIONS");
	res.header("Access-Control-Allow-Credentials", true);
	if (req.method == 'OPTIONS') {
		res.sendStatus(200);
	} else {
		next();
	}
})

// json 中间件
app.use(express.json());

// 路由中间件
app.use("/", router);

// 监听端口
app.listen(port, hostname, () => {
  console.log(`http://${hostname}:${port}`)
});