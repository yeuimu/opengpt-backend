const { secret } = require('../config/config')
const User = require('../models/user')
const jwt = require('jsonwebtoken')

module.exports = async function token (token) {
  try {
    const { id } = jwt.verify(token, secret)
    const user = await User.findById(id)
    const { username } = user
    return username
  } catch (error) {
    console.error(error)
    throw new Error('token 验证失败！')
  }
}
