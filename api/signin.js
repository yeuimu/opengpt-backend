const User = require('../models/user')

module.exports = async function signin (req, res, next) {
  console.log(req.body)
  try {
    await User.create({
      username: req.body.username,
      password: req.body.password
    })
    res.send({
      message: '用户创建成功！'
    })
  } catch (error) {
    console.error('创建失败')
    return res.status(422).send({
      message: '用户名已经存在！'
    })
  }
}
